pipeline {

    agent any

    tools {
        maven 'mvn 3.9.5'
    }

    environment {
        SONAR_SCANNER = tool 'SonarScanner'
        JENKINS_SERVICE = credentials('service-jenkins')
    }

    stages {

        stage('Compilar') {
            steps {
                echo 'Compilando...'
                sh "mvn clean compile"
            }
        }

        stage('Pruebas') {
            steps {
                echo 'Ejecutando pruebas...'
                sh "mvn test -Dspring.profiles.active=test"
            }
        }

        stage('Análisis de Código Estático') {
            steps {
                echo 'Analizando código....'

                withSonarQubeEnv('sonarqube-server') {
                    sh "${SONAR_SCANNER}/bin/sonar-scanner -Dproject.settings=devops/sonar.properties"
                }

                timeout(time: 5, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }

            }
        }

        stage('Build') {
            steps {
                echo 'Creando el build...'
                sh "mvn package -DskipTests"
                sh "docker build -t us-central1-docker.pkg.dev/devops-tallerfinal/develop/servicio-seguridad-taller-final:$BUILD_NUMBER"
            }
        }

        stage('Publicar Docker Hub') {
            steps {
                echo 'Publicando imagen...'
                sh "docker login -u _json_key --password-stdin https://us-central1-docker.pkg.dev < $JENKINS_SERVICE"
                sh "docker push us-central1-docker.pkg.dev/devops-tallerfinal/develop/servicio-seguridad-taller-final:$BUILD_NUMBER"
            }
            post {
                always {
                    echo "eliminando imagen..."
                    sh "docker rmi us-central1-docker.pkg.dev/devops-tallerfinal/develop/servicio-seguridad-taller-final:$BUILD_NUMBER"
                }
            }
        }

        stage('Desplegar') {
            steps {
                echo 'Desplegando...'
                sh "gcloud auth activate-service-account jenkins-service@devops-devops-tallerfinal.iam.gserviceaccount.com --key-file=$JENKINS_SERVICE"
                sh "gcloud run deploy servicio-seguridad --image=us-central1-docker.pkg.dev/devops-tallerfinal/develop/servicio-seguridad-taller:$BUILD_NUMBER --region=us-central1 --port=8080 --project=devops-tallerfinal --allow-unauthenticated"
            }
        }

    }

}