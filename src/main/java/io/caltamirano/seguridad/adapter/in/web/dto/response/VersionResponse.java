package io.caltamirano.seguridad.adapter.in.web.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VersionResponse {

	public VersionResponse(String string, String string2) {
		// TODO Auto-generated constructor stub
	}

	private String minor;
	
	private String major;
	
}
